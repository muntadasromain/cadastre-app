import '../style/loader.css';

function Loader() {
    return (
        <div className="loader-container">
            <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
            <span className="loader-label">Chargement...</span>
        </div>
    )
}

export default Loader;