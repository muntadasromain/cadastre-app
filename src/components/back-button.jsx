import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

function BackButton({ label, onClick }) {

    return (
        <div className="back-button" onClick={onClick}>
            <FontAwesomeIcon icon="fa-solid fa-chevron-left" />
            <span>{label}</span>
        </div>
    );
}

export default BackButton;