import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { useEffect, useState } from "react"
import { Bar, BarChart, Tooltip, XAxis, YAxis } from "recharts"

function BarGraph({ data, title, dataKey, xAxis, width, onRefresh }) {

    const [isRefreshing, setIsRefreshing] = useState(false)

    const refreshGraph = () => {
        if (isRefreshing) return
        setIsRefreshing(true)
        onRefresh()
    }

    useEffect(() => {
        setIsRefreshing(false)
    }, [data])

    return (
        <div className="padel-component">
            <div className="padel-component-header">
                <h2>{title}</h2>
                <FontAwesomeIcon className="clickable-icon" icon="fa-solid fa-arrows-rotate" onClick={refreshGraph} spin={isRefreshing} />
            </div>
            <BarChart data={data} width={width} height={400} >
                <Tooltip />
                <XAxis dataKey={xAxis} />
                <YAxis />
                <Bar fill="rgb(0, 136, 204)" dataKey={dataKey} />
            </BarChart>
        </div>
    )
}

export default BarGraph