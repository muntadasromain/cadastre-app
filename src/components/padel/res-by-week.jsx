import PadelServices from "../../services/padel";
import { Line, LineChart, Tooltip, XAxis, YAxis } from "recharts";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import chartColors from '../../assets/chart-colors.json'

const ResByWeek = ({ width, clubIds, data, refreshing, onRefresh }) => {

    return (
        <div className="padel-component">
            <div className="padel-component-header">
                <h2>Reservations par semaine</h2>
                <FontAwesomeIcon className="clickable-icon" icon="fa-solid fa-arrows-rotate" onClick={onRefresh} spin={refreshing} />
            </div>
            {refreshing ? <div className="loading">Chargement...</div>
                : <LineChart data={data} width={width} height={300} >
                    <Tooltip />
                    <XAxis dataKey={"yearweek"} />
                    <YAxis />
                    {clubIds && clubIds.length > 0 && clubIds.map((clubId, index) => (
                        <Line type='monotone' key={index} fill={chartColors[index % 5].color} dataKey={PadelServices.getClubNameFromId(clubId)} />
                    ))}
                    {!clubIds || clubIds.length === 0 ?
                        <Line type='monotone' fill="rgb(0, 136, 204)" dataKey="Tout club" />
                        : null
                    }
                </LineChart>
            }
        </div>
    )

}

export default ResByWeek