import { useState } from 'react'
import { Listbox } from '@headlessui/react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

function FilterSelect({ selectedOption, options, onChange }) {

    const [filterUp, setFilterUp] = useState(true)

    const selectedStyle = {
        backgroundColor: 'white',
        height: '24px',
        borderRadius: '12px',
        border: "1px solid rgb(var(--black-1))",
        padding: "4px 12px",
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        cursor: "pointer",
    }

    const optionsStyle = {
        position: "absolute",
        backgroundColor: "white",
        listStyleType: "none",
        padding: "4px 0px",
        fontSize: "0.8rem",
        margin: 0,
        marginTop: "4px",
        marginRight: "8px",
        borderRadius: "12px",
        cursor: "pointer",
        border: "1px solid rgb(var(--black-1))",
    }

    const filterDirectionStyle = {
        height: '14px',
        cursor: 'pointer',
        margin: "0px",
    }

    return (
        <Listbox value={selectedOption} onChange={(option) => { onChange(option.id) }}>
            <div>
                <Listbox.Button style={selectedStyle} >
                    {options[selectedOption].label}
                    <div style={{ width: "0px", height: "16px", borderLeft: "1px solid rgb(var(--black-3))", margin: "0px 6px" }}></div>
                    <FontAwesomeIcon onClick={(e) => { e.stopPropagation(); setFilterUp(!filterUp) }} style={filterDirectionStyle} icon={"fa-solid fa-" + (filterUp ? "arrow-up-short-wide" : "arrow-down-wide-short")} />
                </Listbox.Button>
                <Listbox.Options style={optionsStyle} >
                    {options.map((option) => (
                        <Listbox.Option className="filter-option" key={option.id} value={option}>
                            {({ selected, active }) => (
                                <div className={`flex items-center ${selected ? 'bg-blue-600 text-white' : 'text-gray-900'}`}>
                                    <span className={`block truncate ${active ? 'font-semibold' : 'font-normal'}`}>
                                        {option.label}
                                    </span>
                                </div>
                            )}
                        </Listbox.Option>
                    ))}
                </Listbox.Options>
            </div>
        </Listbox >
    )

}

export default FilterSelect;