import { useEffect, useRef, useState } from "react"

import '../style/collection.css'

function CollectionTabs({ defaultActive, onTabChange }) {

    const [activeTab, setActiveTab] = useState(defaultActive ? defaultActive : 'collection-tab-mine')

    const collectionTabs = useRef(null);

    useEffect(() => {
        if (collectionTabs && collectionTabs.current) {
            collectionTabs.current.childNodes.forEach((li) => {
                if (li.id === activeTab) li.classList.add('active')
                else li.classList.remove('active')
            })
        }
    }, [activeTab]);

    const containerStyle = {
        height: '48px',
        widht: '100%',
        display: 'flex',
        padding: '0px',
        listStyleType: 'none',
        alignItems: 'center',
        justifyContent: 'space-around',
        margin: 0
    }


    function onClick({ target }) {
        setActiveTab(target.id)
        onTabChange(target.id)
    }

    return (
        <ul ref={collectionTabs} id="collection-tabs" style={containerStyle}>
            <li className="active" id='collection-tab-mine' onClick={onClick}>Mes collections</li>
            <li id='collection-tab-shared' onClick={onClick}>Partagées avec moi</li>
            <li id='collection-tab-explore' onClick={onClick}>Explorer</li>
        </ul>
    )
}

export default CollectionTabs;