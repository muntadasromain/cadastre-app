
-- Reservations par jour de la semaine
SELECT DAYOFWEEK(r.datetime) AS day_of_week,
       COUNT(*) AS reservations_count
FROM reservation r
INNER JOIN court c ON r.court = c.id
AND c.club = :club_id
WHERE r.available = 0 -- Only consider unavailable timeslots
AND r.id = (
    SELECT MAX(id) -- Subquery to get the highest id for each datetime and courtId
    FROM reservation
    WHERE court = r.court
    AND datetime = r.datetime
)
AND r.datetime >= :start_datetime -- Specify start of time interval
AND r.datetime <= :end_datetime -- Specify end of time interval
GROUP BY DAYOFWEEK(r.datetime);


-- Reservation par creneau horaire
SELECT SUBSTRING(r.datetime, 11, 6) AS creneau,
       COUNT(*) AS reservations_count
FROM reservation r
INNER JOIN court c ON r.court = c.id
AND c.club = ${clubId}
WHERE r.available = 0
AND r.id = (
    SELECT MAX(id)
    FROM reservation
    WHERE court = r.court
    AND datetime = r.datetime
)
AND r.datetime >= ${start}
AND r.datetime <= ${end}


-- taux de remplissage
SELECT
    (
        SELECT COUNT(*)
        FROM reservation r1
        INNER JOIN court c1 ON r1.court = c1.id
        WHERE r1.id = (
            SELECT MAX(id)
            FROM reservation
            WHERE court = r1.court
            AND datetime = r1.datetime
        )
        AND r1.datetime >= ${start}
        AND r1.datetime <= ${end}
        AND c1.club = ${clubId}
    ) AS max_reservations,
    (
        SELECT COUNT(*)
        FROM reservation r2
        INNER JOIN court c2 ON r2.court = c2.id
        WHERE r2.id = (
            SELECT MAX(id)
            FROM reservation
            WHERE court = r2.court
            AND datetime = r2.datetime
        )
        AND r2.datetime >= ${start}
        AND r2.datetime <= ${end}
        AND c2.club = ${clubId}
        AND r2.available = 1
    ) AS available_timeslots;

-- reservations par semaines
SELECT YEARWEEK(r.datetime) as year_week, count(*) as reservation_amount FROM reservation r
INNER JOIN court c ON r.court = c.id
WHERE r.id = (
	SELECT MAX(id)
    FROM reservation
    WHERE court = r.court
    AND datetime = r.datetime
)
WHERE c.club = ${clubId}
AND r.datetime >= "${start}"
AND r.datetime <= "${end}"
GROUP BY WEEK(r.datetime)